// Generated by gencpp from file kbot_bridge/SonarPing.msg
// DO NOT EDIT!


#ifndef KBOT_BRIDGE_MESSAGE_SONARPING_H
#define KBOT_BRIDGE_MESSAGE_SONARPING_H


#include <string>
#include <vector>
#include <map>

#include <ros/types.h>
#include <ros/serialization.h>
#include <ros/builtin_message_traits.h>
#include <ros/message_operations.h>

#include <std_msgs/Header.h>
#include <geometry_msgs/Pose.h>
#include <sensor_msgs/Range.h>

namespace kbot_bridge
{
template <class ContainerAllocator>
struct SonarPing_
{
  typedef SonarPing_<ContainerAllocator> Type;

  SonarPing_()
    : header()
    , pose()
    , range()  {
    }
  SonarPing_(const ContainerAllocator& _alloc)
    : header(_alloc)
    , pose(_alloc)
    , range(_alloc)  {
  (void)_alloc;
    }



   typedef  ::std_msgs::Header_<ContainerAllocator>  _header_type;
  _header_type header;

   typedef  ::geometry_msgs::Pose_<ContainerAllocator>  _pose_type;
  _pose_type pose;

   typedef  ::sensor_msgs::Range_<ContainerAllocator>  _range_type;
  _range_type range;





  typedef boost::shared_ptr< ::kbot_bridge::SonarPing_<ContainerAllocator> > Ptr;
  typedef boost::shared_ptr< ::kbot_bridge::SonarPing_<ContainerAllocator> const> ConstPtr;

}; // struct SonarPing_

typedef ::kbot_bridge::SonarPing_<std::allocator<void> > SonarPing;

typedef boost::shared_ptr< ::kbot_bridge::SonarPing > SonarPingPtr;
typedef boost::shared_ptr< ::kbot_bridge::SonarPing const> SonarPingConstPtr;

// constants requiring out of line definition



template<typename ContainerAllocator>
std::ostream& operator<<(std::ostream& s, const ::kbot_bridge::SonarPing_<ContainerAllocator> & v)
{
ros::message_operations::Printer< ::kbot_bridge::SonarPing_<ContainerAllocator> >::stream(s, "", v);
return s;
}

} // namespace kbot_bridge

namespace ros
{
namespace message_traits
{



// BOOLTRAITS {'IsFixedSize': False, 'IsMessage': True, 'HasHeader': True}
// {'sensor_msgs': ['/opt/ros/kinetic/share/sensor_msgs/cmake/../msg'], 'kbot_bridge': ['/home/adol/catkin_ws/src/kbot/kbot_bridge/msg'], 'std_msgs': ['/opt/ros/kinetic/share/std_msgs/cmake/../msg'], 'geometry_msgs': ['/opt/ros/kinetic/share/geometry_msgs/cmake/../msg']}

// !!!!!!!!!!! ['__class__', '__delattr__', '__dict__', '__doc__', '__eq__', '__format__', '__getattribute__', '__hash__', '__init__', '__module__', '__ne__', '__new__', '__reduce__', '__reduce_ex__', '__repr__', '__setattr__', '__sizeof__', '__str__', '__subclasshook__', '__weakref__', '_parsed_fields', 'constants', 'fields', 'full_name', 'has_header', 'header_present', 'names', 'package', 'parsed_fields', 'short_name', 'text', 'types']




template <class ContainerAllocator>
struct IsFixedSize< ::kbot_bridge::SonarPing_<ContainerAllocator> >
  : FalseType
  { };

template <class ContainerAllocator>
struct IsFixedSize< ::kbot_bridge::SonarPing_<ContainerAllocator> const>
  : FalseType
  { };

template <class ContainerAllocator>
struct IsMessage< ::kbot_bridge::SonarPing_<ContainerAllocator> >
  : TrueType
  { };

template <class ContainerAllocator>
struct IsMessage< ::kbot_bridge::SonarPing_<ContainerAllocator> const>
  : TrueType
  { };

template <class ContainerAllocator>
struct HasHeader< ::kbot_bridge::SonarPing_<ContainerAllocator> >
  : TrueType
  { };

template <class ContainerAllocator>
struct HasHeader< ::kbot_bridge::SonarPing_<ContainerAllocator> const>
  : TrueType
  { };


template<class ContainerAllocator>
struct MD5Sum< ::kbot_bridge::SonarPing_<ContainerAllocator> >
{
  static const char* value()
  {
    return "0b401c512e00be0203ad9244351ac7a5";
  }

  static const char* value(const ::kbot_bridge::SonarPing_<ContainerAllocator>&) { return value(); }
  static const uint64_t static_value1 = 0x0b401c512e00be02ULL;
  static const uint64_t static_value2 = 0x03ad9244351ac7a5ULL;
};

template<class ContainerAllocator>
struct DataType< ::kbot_bridge::SonarPing_<ContainerAllocator> >
{
  static const char* value()
  {
    return "kbot_bridge/SonarPing";
  }

  static const char* value(const ::kbot_bridge::SonarPing_<ContainerAllocator>&) { return value(); }
};

template<class ContainerAllocator>
struct Definition< ::kbot_bridge::SonarPing_<ContainerAllocator> >
{
  static const char* value()
  {
    return "#Sonar range measurement with sensor pose\n\
Header header\n\
geometry_msgs/Pose pose\n\
sensor_msgs/Range range\n\
\n\
================================================================================\n\
MSG: std_msgs/Header\n\
# Standard metadata for higher-level stamped data types.\n\
# This is generally used to communicate timestamped data \n\
# in a particular coordinate frame.\n\
# \n\
# sequence ID: consecutively increasing ID \n\
uint32 seq\n\
#Two-integer timestamp that is expressed as:\n\
# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')\n\
# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')\n\
# time-handling sugar is provided by the client library\n\
time stamp\n\
#Frame this data is associated with\n\
# 0: no frame\n\
# 1: global frame\n\
string frame_id\n\
\n\
================================================================================\n\
MSG: geometry_msgs/Pose\n\
# A representation of pose in free space, composed of position and orientation. \n\
Point position\n\
Quaternion orientation\n\
\n\
================================================================================\n\
MSG: geometry_msgs/Point\n\
# This contains the position of a point in free space\n\
float64 x\n\
float64 y\n\
float64 z\n\
\n\
================================================================================\n\
MSG: geometry_msgs/Quaternion\n\
# This represents an orientation in free space in quaternion form.\n\
\n\
float64 x\n\
float64 y\n\
float64 z\n\
float64 w\n\
\n\
================================================================================\n\
MSG: sensor_msgs/Range\n\
# Single range reading from an active ranger that emits energy and reports\n\
# one range reading that is valid along an arc at the distance measured. \n\
# This message is  not appropriate for laser scanners. See the LaserScan\n\
# message if you are working with a laser scanner.\n\
\n\
# This message also can represent a fixed-distance (binary) ranger.  This\n\
# sensor will have min_range===max_range===distance of detection.\n\
# These sensors follow REP 117 and will output -Inf if the object is detected\n\
# and +Inf if the object is outside of the detection range.\n\
\n\
Header header           # timestamp in the header is the time the ranger\n\
                        # returned the distance reading\n\
\n\
# Radiation type enums\n\
# If you want a value added to this list, send an email to the ros-users list\n\
uint8 ULTRASOUND=0\n\
uint8 INFRARED=1\n\
\n\
uint8 radiation_type    # the type of radiation used by the sensor\n\
                        # (sound, IR, etc) [enum]\n\
\n\
float32 field_of_view   # the size of the arc that the distance reading is\n\
                        # valid for [rad]\n\
                        # the object causing the range reading may have\n\
                        # been anywhere within -field_of_view/2 and\n\
                        # field_of_view/2 at the measured range. \n\
                        # 0 angle corresponds to the x-axis of the sensor.\n\
\n\
float32 min_range       # minimum range value [m]\n\
float32 max_range       # maximum range value [m]\n\
                        # Fixed distance rangers require min_range==max_range\n\
\n\
float32 range           # range data [m]\n\
                        # (Note: values < range_min or > range_max\n\
                        # should be discarded)\n\
                        # Fixed distance rangers only output -Inf or +Inf.\n\
                        # -Inf represents a detection within fixed distance.\n\
                        # (Detection too close to the sensor to quantify)\n\
                        # +Inf represents no detection within the fixed distance.\n\
                        # (Object out of range)\n\
";
  }

  static const char* value(const ::kbot_bridge::SonarPing_<ContainerAllocator>&) { return value(); }
};

} // namespace message_traits
} // namespace ros

namespace ros
{
namespace serialization
{

  template<class ContainerAllocator> struct Serializer< ::kbot_bridge::SonarPing_<ContainerAllocator> >
  {
    template<typename Stream, typename T> inline static void allInOne(Stream& stream, T m)
    {
      stream.next(m.header);
      stream.next(m.pose);
      stream.next(m.range);
    }

    ROS_DECLARE_ALLINONE_SERIALIZER
  }; // struct SonarPing_

} // namespace serialization
} // namespace ros

namespace ros
{
namespace message_operations
{

template<class ContainerAllocator>
struct Printer< ::kbot_bridge::SonarPing_<ContainerAllocator> >
{
  template<typename Stream> static void stream(Stream& s, const std::string& indent, const ::kbot_bridge::SonarPing_<ContainerAllocator>& v)
  {
    s << indent << "header: ";
    s << std::endl;
    Printer< ::std_msgs::Header_<ContainerAllocator> >::stream(s, indent + "  ", v.header);
    s << indent << "pose: ";
    s << std::endl;
    Printer< ::geometry_msgs::Pose_<ContainerAllocator> >::stream(s, indent + "  ", v.pose);
    s << indent << "range: ";
    s << std::endl;
    Printer< ::sensor_msgs::Range_<ContainerAllocator> >::stream(s, indent + "  ", v.range);
  }
};

} // namespace message_operations
} // namespace ros

#endif // KBOT_BRIDGE_MESSAGE_SONARPING_H
