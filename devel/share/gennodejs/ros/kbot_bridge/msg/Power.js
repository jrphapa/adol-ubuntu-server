// Auto-generated. Do not edit!

// (in-package kbot_bridge.msg)


"use strict";

const _serializer = _ros_msg_utils.Serialize;
const _arraySerializer = _serializer.Array;
const _deserializer = _ros_msg_utils.Deserialize;
const _arrayDeserializer = _deserializer.Array;
const _finder = _ros_msg_utils.Find;
const _getByteLength = _ros_msg_utils.getByteLength;
let std_msgs = _finder('std_msgs');

//-----------------------------------------------------------

class Power {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.header = null;
      this.mainVoltage = null;
      this.mainVoltageDiff = null;
      this.mainCurrent = null;
      this.mainCurrentDiff = null;
      this.secondaryVoltage = null;
      this.mainStatus = null;
      this.status = null;
      this.charging = null;
    }
    else {
      if (initObj.hasOwnProperty('header')) {
        this.header = initObj.header
      }
      else {
        this.header = new std_msgs.msg.Header();
      }
      if (initObj.hasOwnProperty('mainVoltage')) {
        this.mainVoltage = initObj.mainVoltage
      }
      else {
        this.mainVoltage = 0.0;
      }
      if (initObj.hasOwnProperty('mainVoltageDiff')) {
        this.mainVoltageDiff = initObj.mainVoltageDiff
      }
      else {
        this.mainVoltageDiff = 0.0;
      }
      if (initObj.hasOwnProperty('mainCurrent')) {
        this.mainCurrent = initObj.mainCurrent
      }
      else {
        this.mainCurrent = 0.0;
      }
      if (initObj.hasOwnProperty('mainCurrentDiff')) {
        this.mainCurrentDiff = initObj.mainCurrentDiff
      }
      else {
        this.mainCurrentDiff = 0.0;
      }
      if (initObj.hasOwnProperty('secondaryVoltage')) {
        this.secondaryVoltage = initObj.secondaryVoltage
      }
      else {
        this.secondaryVoltage = 0.0;
      }
      if (initObj.hasOwnProperty('mainStatus')) {
        this.mainStatus = initObj.mainStatus
      }
      else {
        this.mainStatus = 0;
      }
      if (initObj.hasOwnProperty('status')) {
        this.status = initObj.status
      }
      else {
        this.status = 0;
      }
      if (initObj.hasOwnProperty('charging')) {
        this.charging = initObj.charging
      }
      else {
        this.charging = 0;
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type Power
    // Serialize message field [header]
    bufferOffset = std_msgs.msg.Header.serialize(obj.header, buffer, bufferOffset);
    // Serialize message field [mainVoltage]
    bufferOffset = _serializer.float32(obj.mainVoltage, buffer, bufferOffset);
    // Serialize message field [mainVoltageDiff]
    bufferOffset = _serializer.float32(obj.mainVoltageDiff, buffer, bufferOffset);
    // Serialize message field [mainCurrent]
    bufferOffset = _serializer.float32(obj.mainCurrent, buffer, bufferOffset);
    // Serialize message field [mainCurrentDiff]
    bufferOffset = _serializer.float32(obj.mainCurrentDiff, buffer, bufferOffset);
    // Serialize message field [secondaryVoltage]
    bufferOffset = _serializer.float32(obj.secondaryVoltage, buffer, bufferOffset);
    // Serialize message field [mainStatus]
    bufferOffset = _serializer.uint8(obj.mainStatus, buffer, bufferOffset);
    // Serialize message field [status]
    bufferOffset = _serializer.uint8(obj.status, buffer, bufferOffset);
    // Serialize message field [charging]
    bufferOffset = _serializer.uint8(obj.charging, buffer, bufferOffset);
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type Power
    let len;
    let data = new Power(null);
    // Deserialize message field [header]
    data.header = std_msgs.msg.Header.deserialize(buffer, bufferOffset);
    // Deserialize message field [mainVoltage]
    data.mainVoltage = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [mainVoltageDiff]
    data.mainVoltageDiff = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [mainCurrent]
    data.mainCurrent = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [mainCurrentDiff]
    data.mainCurrentDiff = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [secondaryVoltage]
    data.secondaryVoltage = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [mainStatus]
    data.mainStatus = _deserializer.uint8(buffer, bufferOffset);
    // Deserialize message field [status]
    data.status = _deserializer.uint8(buffer, bufferOffset);
    // Deserialize message field [charging]
    data.charging = _deserializer.uint8(buffer, bufferOffset);
    return data;
  }

  static getMessageSize(object) {
    let length = 0;
    length += std_msgs.msg.Header.getMessageSize(object.header);
    return length + 23;
  }

  static datatype() {
    // Returns string type for a message object
    return 'kbot_bridge/Power';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return '1cdb5b77dcdecba256df78fcd21edf76';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    #
    Header header
    float32 mainVoltage
    float32 mainVoltageDiff
    float32 mainCurrent
    float32 mainCurrentDiff
    float32 secondaryVoltage
    uint8 mainStatus
    uint8 status
    uint8 charging
    
    ================================================================================
    MSG: std_msgs/Header
    # Standard metadata for higher-level stamped data types.
    # This is generally used to communicate timestamped data 
    # in a particular coordinate frame.
    # 
    # sequence ID: consecutively increasing ID 
    uint32 seq
    #Two-integer timestamp that is expressed as:
    # * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')
    # * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')
    # time-handling sugar is provided by the client library
    time stamp
    #Frame this data is associated with
    # 0: no frame
    # 1: global frame
    string frame_id
    
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new Power(null);
    if (msg.header !== undefined) {
      resolved.header = std_msgs.msg.Header.Resolve(msg.header)
    }
    else {
      resolved.header = new std_msgs.msg.Header()
    }

    if (msg.mainVoltage !== undefined) {
      resolved.mainVoltage = msg.mainVoltage;
    }
    else {
      resolved.mainVoltage = 0.0
    }

    if (msg.mainVoltageDiff !== undefined) {
      resolved.mainVoltageDiff = msg.mainVoltageDiff;
    }
    else {
      resolved.mainVoltageDiff = 0.0
    }

    if (msg.mainCurrent !== undefined) {
      resolved.mainCurrent = msg.mainCurrent;
    }
    else {
      resolved.mainCurrent = 0.0
    }

    if (msg.mainCurrentDiff !== undefined) {
      resolved.mainCurrentDiff = msg.mainCurrentDiff;
    }
    else {
      resolved.mainCurrentDiff = 0.0
    }

    if (msg.secondaryVoltage !== undefined) {
      resolved.secondaryVoltage = msg.secondaryVoltage;
    }
    else {
      resolved.secondaryVoltage = 0.0
    }

    if (msg.mainStatus !== undefined) {
      resolved.mainStatus = msg.mainStatus;
    }
    else {
      resolved.mainStatus = 0
    }

    if (msg.status !== undefined) {
      resolved.status = msg.status;
    }
    else {
      resolved.status = 0
    }

    if (msg.charging !== undefined) {
      resolved.charging = msg.charging;
    }
    else {
      resolved.charging = 0
    }

    return resolved;
    }
};

module.exports = Power;
