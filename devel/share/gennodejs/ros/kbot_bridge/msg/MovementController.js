// Auto-generated. Do not edit!

// (in-package kbot_bridge.msg)


"use strict";

const _serializer = _ros_msg_utils.Serialize;
const _arraySerializer = _serializer.Array;
const _deserializer = _ros_msg_utils.Deserialize;
const _arrayDeserializer = _deserializer.Array;
const _finder = _ros_msg_utils.Find;
const _getByteLength = _ros_msg_utils.getByteLength;
let std_msgs = _finder('std_msgs');

//-----------------------------------------------------------

class MovementController {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.header = null;
      this.taskID = null;
      this.taskName = null;
      this.taskPriority = null;
      this.forward = null;
      this.turn = null;
    }
    else {
      if (initObj.hasOwnProperty('header')) {
        this.header = initObj.header
      }
      else {
        this.header = new std_msgs.msg.Header();
      }
      if (initObj.hasOwnProperty('taskID')) {
        this.taskID = initObj.taskID
      }
      else {
        this.taskID = 0;
      }
      if (initObj.hasOwnProperty('taskName')) {
        this.taskName = initObj.taskName
      }
      else {
        this.taskName = '';
      }
      if (initObj.hasOwnProperty('taskPriority')) {
        this.taskPriority = initObj.taskPriority
      }
      else {
        this.taskPriority = 0;
      }
      if (initObj.hasOwnProperty('forward')) {
        this.forward = initObj.forward
      }
      else {
        this.forward = 0;
      }
      if (initObj.hasOwnProperty('turn')) {
        this.turn = initObj.turn
      }
      else {
        this.turn = 0;
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type MovementController
    // Serialize message field [header]
    bufferOffset = std_msgs.msg.Header.serialize(obj.header, buffer, bufferOffset);
    // Serialize message field [taskID]
    bufferOffset = _serializer.uint8(obj.taskID, buffer, bufferOffset);
    // Serialize message field [taskName]
    bufferOffset = _serializer.string(obj.taskName, buffer, bufferOffset);
    // Serialize message field [taskPriority]
    bufferOffset = _serializer.uint8(obj.taskPriority, buffer, bufferOffset);
    // Serialize message field [forward]
    bufferOffset = _serializer.uint16(obj.forward, buffer, bufferOffset);
    // Serialize message field [turn]
    bufferOffset = _serializer.uint16(obj.turn, buffer, bufferOffset);
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type MovementController
    let len;
    let data = new MovementController(null);
    // Deserialize message field [header]
    data.header = std_msgs.msg.Header.deserialize(buffer, bufferOffset);
    // Deserialize message field [taskID]
    data.taskID = _deserializer.uint8(buffer, bufferOffset);
    // Deserialize message field [taskName]
    data.taskName = _deserializer.string(buffer, bufferOffset);
    // Deserialize message field [taskPriority]
    data.taskPriority = _deserializer.uint8(buffer, bufferOffset);
    // Deserialize message field [forward]
    data.forward = _deserializer.uint16(buffer, bufferOffset);
    // Deserialize message field [turn]
    data.turn = _deserializer.uint16(buffer, bufferOffset);
    return data;
  }

  static getMessageSize(object) {
    let length = 0;
    length += std_msgs.msg.Header.getMessageSize(object.header);
    length += object.taskName.length;
    return length + 10;
  }

  static datatype() {
    // Returns string type for a message object
    return 'kbot_bridge/MovementController';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return '5202306735651b0fbe3bb3c7dbbca1fd';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    #
    Header header
    uint8 taskID
    string taskName
    uint8 taskPriority
    uint16 forward
    uint16 turn
    
    ================================================================================
    MSG: std_msgs/Header
    # Standard metadata for higher-level stamped data types.
    # This is generally used to communicate timestamped data 
    # in a particular coordinate frame.
    # 
    # sequence ID: consecutively increasing ID 
    uint32 seq
    #Two-integer timestamp that is expressed as:
    # * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')
    # * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')
    # time-handling sugar is provided by the client library
    time stamp
    #Frame this data is associated with
    # 0: no frame
    # 1: global frame
    string frame_id
    
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new MovementController(null);
    if (msg.header !== undefined) {
      resolved.header = std_msgs.msg.Header.Resolve(msg.header)
    }
    else {
      resolved.header = new std_msgs.msg.Header()
    }

    if (msg.taskID !== undefined) {
      resolved.taskID = msg.taskID;
    }
    else {
      resolved.taskID = 0
    }

    if (msg.taskName !== undefined) {
      resolved.taskName = msg.taskName;
    }
    else {
      resolved.taskName = ''
    }

    if (msg.taskPriority !== undefined) {
      resolved.taskPriority = msg.taskPriority;
    }
    else {
      resolved.taskPriority = 0
    }

    if (msg.forward !== undefined) {
      resolved.forward = msg.forward;
    }
    else {
      resolved.forward = 0
    }

    if (msg.turn !== undefined) {
      resolved.turn = msg.turn;
    }
    else {
      resolved.turn = 0
    }

    return resolved;
    }
};

module.exports = MovementController;
