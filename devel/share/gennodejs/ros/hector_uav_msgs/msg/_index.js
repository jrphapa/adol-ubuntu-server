
"use strict";

let PositionXYCommand = require('./PositionXYCommand.js');
let RawRC = require('./RawRC.js');
let VelocityXYCommand = require('./VelocityXYCommand.js');
let ControllerState = require('./ControllerState.js');
let RuddersCommand = require('./RuddersCommand.js');
let Altimeter = require('./Altimeter.js');
let MotorCommand = require('./MotorCommand.js');
let Supply = require('./Supply.js');
let RawImu = require('./RawImu.js');
let HeightCommand = require('./HeightCommand.js');
let Compass = require('./Compass.js');
let ThrustCommand = require('./ThrustCommand.js');
let MotorStatus = require('./MotorStatus.js');
let VelocityZCommand = require('./VelocityZCommand.js');
let AttitudeCommand = require('./AttitudeCommand.js');
let YawrateCommand = require('./YawrateCommand.js');
let RawMagnetic = require('./RawMagnetic.js');
let MotorPWM = require('./MotorPWM.js');
let RC = require('./RC.js');
let ServoCommand = require('./ServoCommand.js');
let HeadingCommand = require('./HeadingCommand.js');
let LandingActionResult = require('./LandingActionResult.js');
let LandingActionFeedback = require('./LandingActionFeedback.js');
let LandingGoal = require('./LandingGoal.js');
let PoseGoal = require('./PoseGoal.js');
let PoseActionResult = require('./PoseActionResult.js');
let PoseResult = require('./PoseResult.js');
let TakeoffActionFeedback = require('./TakeoffActionFeedback.js');
let TakeoffAction = require('./TakeoffAction.js');
let TakeoffGoal = require('./TakeoffGoal.js');
let TakeoffResult = require('./TakeoffResult.js');
let TakeoffActionGoal = require('./TakeoffActionGoal.js');
let TakeoffFeedback = require('./TakeoffFeedback.js');
let PoseActionGoal = require('./PoseActionGoal.js');
let LandingAction = require('./LandingAction.js');
let LandingActionGoal = require('./LandingActionGoal.js');
let TakeoffActionResult = require('./TakeoffActionResult.js');
let LandingResult = require('./LandingResult.js');
let PoseAction = require('./PoseAction.js');
let LandingFeedback = require('./LandingFeedback.js');
let PoseActionFeedback = require('./PoseActionFeedback.js');
let PoseFeedback = require('./PoseFeedback.js');

module.exports = {
  PositionXYCommand: PositionXYCommand,
  RawRC: RawRC,
  VelocityXYCommand: VelocityXYCommand,
  ControllerState: ControllerState,
  RuddersCommand: RuddersCommand,
  Altimeter: Altimeter,
  MotorCommand: MotorCommand,
  Supply: Supply,
  RawImu: RawImu,
  HeightCommand: HeightCommand,
  Compass: Compass,
  ThrustCommand: ThrustCommand,
  MotorStatus: MotorStatus,
  VelocityZCommand: VelocityZCommand,
  AttitudeCommand: AttitudeCommand,
  YawrateCommand: YawrateCommand,
  RawMagnetic: RawMagnetic,
  MotorPWM: MotorPWM,
  RC: RC,
  ServoCommand: ServoCommand,
  HeadingCommand: HeadingCommand,
  LandingActionResult: LandingActionResult,
  LandingActionFeedback: LandingActionFeedback,
  LandingGoal: LandingGoal,
  PoseGoal: PoseGoal,
  PoseActionResult: PoseActionResult,
  PoseResult: PoseResult,
  TakeoffActionFeedback: TakeoffActionFeedback,
  TakeoffAction: TakeoffAction,
  TakeoffGoal: TakeoffGoal,
  TakeoffResult: TakeoffResult,
  TakeoffActionGoal: TakeoffActionGoal,
  TakeoffFeedback: TakeoffFeedback,
  PoseActionGoal: PoseActionGoal,
  LandingAction: LandingAction,
  LandingActionGoal: LandingActionGoal,
  TakeoffActionResult: TakeoffActionResult,
  LandingResult: LandingResult,
  PoseAction: PoseAction,
  LandingFeedback: LandingFeedback,
  PoseActionFeedback: PoseActionFeedback,
  PoseFeedback: PoseFeedback,
};
