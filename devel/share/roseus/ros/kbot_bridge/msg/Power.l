;; Auto-generated. Do not edit!


(when (boundp 'kbot_bridge::Power)
  (if (not (find-package "KBOT_BRIDGE"))
    (make-package "KBOT_BRIDGE"))
  (shadow 'Power (find-package "KBOT_BRIDGE")))
(unless (find-package "KBOT_BRIDGE::POWER")
  (make-package "KBOT_BRIDGE::POWER"))

(in-package "ROS")
;;//! \htmlinclude Power.msg.html
(if (not (find-package "STD_MSGS"))
  (ros::roseus-add-msgs "std_msgs"))


(defclass kbot_bridge::Power
  :super ros::object
  :slots (_header _mainVoltage _mainVoltageDiff _mainCurrent _mainCurrentDiff _secondaryVoltage _mainStatus _status _charging ))

(defmethod kbot_bridge::Power
  (:init
   (&key
    ((:header __header) (instance std_msgs::Header :init))
    ((:mainVoltage __mainVoltage) 0.0)
    ((:mainVoltageDiff __mainVoltageDiff) 0.0)
    ((:mainCurrent __mainCurrent) 0.0)
    ((:mainCurrentDiff __mainCurrentDiff) 0.0)
    ((:secondaryVoltage __secondaryVoltage) 0.0)
    ((:mainStatus __mainStatus) 0)
    ((:status __status) 0)
    ((:charging __charging) 0)
    )
   (send-super :init)
   (setq _header __header)
   (setq _mainVoltage (float __mainVoltage))
   (setq _mainVoltageDiff (float __mainVoltageDiff))
   (setq _mainCurrent (float __mainCurrent))
   (setq _mainCurrentDiff (float __mainCurrentDiff))
   (setq _secondaryVoltage (float __secondaryVoltage))
   (setq _mainStatus (round __mainStatus))
   (setq _status (round __status))
   (setq _charging (round __charging))
   self)
  (:header
   (&rest __header)
   (if (keywordp (car __header))
       (send* _header __header)
     (progn
       (if __header (setq _header (car __header)))
       _header)))
  (:mainVoltage
   (&optional __mainVoltage)
   (if __mainVoltage (setq _mainVoltage __mainVoltage)) _mainVoltage)
  (:mainVoltageDiff
   (&optional __mainVoltageDiff)
   (if __mainVoltageDiff (setq _mainVoltageDiff __mainVoltageDiff)) _mainVoltageDiff)
  (:mainCurrent
   (&optional __mainCurrent)
   (if __mainCurrent (setq _mainCurrent __mainCurrent)) _mainCurrent)
  (:mainCurrentDiff
   (&optional __mainCurrentDiff)
   (if __mainCurrentDiff (setq _mainCurrentDiff __mainCurrentDiff)) _mainCurrentDiff)
  (:secondaryVoltage
   (&optional __secondaryVoltage)
   (if __secondaryVoltage (setq _secondaryVoltage __secondaryVoltage)) _secondaryVoltage)
  (:mainStatus
   (&optional __mainStatus)
   (if __mainStatus (setq _mainStatus __mainStatus)) _mainStatus)
  (:status
   (&optional __status)
   (if __status (setq _status __status)) _status)
  (:charging
   (&optional __charging)
   (if __charging (setq _charging __charging)) _charging)
  (:serialization-length
   ()
   (+
    ;; std_msgs/Header _header
    (send _header :serialization-length)
    ;; float32 _mainVoltage
    4
    ;; float32 _mainVoltageDiff
    4
    ;; float32 _mainCurrent
    4
    ;; float32 _mainCurrentDiff
    4
    ;; float32 _secondaryVoltage
    4
    ;; uint8 _mainStatus
    1
    ;; uint8 _status
    1
    ;; uint8 _charging
    1
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; std_msgs/Header _header
       (send _header :serialize s)
     ;; float32 _mainVoltage
       (sys::poke _mainVoltage (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _mainVoltageDiff
       (sys::poke _mainVoltageDiff (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _mainCurrent
       (sys::poke _mainCurrent (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _mainCurrentDiff
       (sys::poke _mainCurrentDiff (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _secondaryVoltage
       (sys::poke _secondaryVoltage (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; uint8 _mainStatus
       (write-byte _mainStatus s)
     ;; uint8 _status
       (write-byte _status s)
     ;; uint8 _charging
       (write-byte _charging s)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; std_msgs/Header _header
     (send _header :deserialize buf ptr-) (incf ptr- (send _header :serialization-length))
   ;; float32 _mainVoltage
     (setq _mainVoltage (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _mainVoltageDiff
     (setq _mainVoltageDiff (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _mainCurrent
     (setq _mainCurrent (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _mainCurrentDiff
     (setq _mainCurrentDiff (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _secondaryVoltage
     (setq _secondaryVoltage (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; uint8 _mainStatus
     (setq _mainStatus (sys::peek buf ptr- :char)) (incf ptr- 1)
   ;; uint8 _status
     (setq _status (sys::peek buf ptr- :char)) (incf ptr- 1)
   ;; uint8 _charging
     (setq _charging (sys::peek buf ptr- :char)) (incf ptr- 1)
   ;;
   self)
  )

(setf (get kbot_bridge::Power :md5sum-) "1cdb5b77dcdecba256df78fcd21edf76")
(setf (get kbot_bridge::Power :datatype-) "kbot_bridge/Power")
(setf (get kbot_bridge::Power :definition-)
      "#
Header header
float32 mainVoltage
float32 mainVoltageDiff
float32 mainCurrent
float32 mainCurrentDiff
float32 secondaryVoltage
uint8 mainStatus
uint8 status
uint8 charging

================================================================================
MSG: std_msgs/Header
# Standard metadata for higher-level stamped data types.
# This is generally used to communicate timestamped data 
# in a particular coordinate frame.
# 
# sequence ID: consecutively increasing ID 
uint32 seq
#Two-integer timestamp that is expressed as:
# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')
# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')
# time-handling sugar is provided by the client library
time stamp
#Frame this data is associated with
# 0: no frame
# 1: global frame
string frame_id

")



(provide :kbot_bridge/Power "1cdb5b77dcdecba256df78fcd21edf76")


