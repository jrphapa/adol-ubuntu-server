;; Auto-generated. Do not edit!


(when (boundp 'kbot_bridge::MovementController)
  (if (not (find-package "KBOT_BRIDGE"))
    (make-package "KBOT_BRIDGE"))
  (shadow 'MovementController (find-package "KBOT_BRIDGE")))
(unless (find-package "KBOT_BRIDGE::MOVEMENTCONTROLLER")
  (make-package "KBOT_BRIDGE::MOVEMENTCONTROLLER"))

(in-package "ROS")
;;//! \htmlinclude MovementController.msg.html
(if (not (find-package "STD_MSGS"))
  (ros::roseus-add-msgs "std_msgs"))


(defclass kbot_bridge::MovementController
  :super ros::object
  :slots (_header _taskID _taskName _taskPriority _forward _turn ))

(defmethod kbot_bridge::MovementController
  (:init
   (&key
    ((:header __header) (instance std_msgs::Header :init))
    ((:taskID __taskID) 0)
    ((:taskName __taskName) "")
    ((:taskPriority __taskPriority) 0)
    ((:forward __forward) 0)
    ((:turn __turn) 0)
    )
   (send-super :init)
   (setq _header __header)
   (setq _taskID (round __taskID))
   (setq _taskName (string __taskName))
   (setq _taskPriority (round __taskPriority))
   (setq _forward (round __forward))
   (setq _turn (round __turn))
   self)
  (:header
   (&rest __header)
   (if (keywordp (car __header))
       (send* _header __header)
     (progn
       (if __header (setq _header (car __header)))
       _header)))
  (:taskID
   (&optional __taskID)
   (if __taskID (setq _taskID __taskID)) _taskID)
  (:taskName
   (&optional __taskName)
   (if __taskName (setq _taskName __taskName)) _taskName)
  (:taskPriority
   (&optional __taskPriority)
   (if __taskPriority (setq _taskPriority __taskPriority)) _taskPriority)
  (:forward
   (&optional __forward)
   (if __forward (setq _forward __forward)) _forward)
  (:turn
   (&optional __turn)
   (if __turn (setq _turn __turn)) _turn)
  (:serialization-length
   ()
   (+
    ;; std_msgs/Header _header
    (send _header :serialization-length)
    ;; uint8 _taskID
    1
    ;; string _taskName
    4 (length _taskName)
    ;; uint8 _taskPriority
    1
    ;; uint16 _forward
    2
    ;; uint16 _turn
    2
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; std_msgs/Header _header
       (send _header :serialize s)
     ;; uint8 _taskID
       (write-byte _taskID s)
     ;; string _taskName
       (write-long (length _taskName) s) (princ _taskName s)
     ;; uint8 _taskPriority
       (write-byte _taskPriority s)
     ;; uint16 _forward
       (write-word _forward s)
     ;; uint16 _turn
       (write-word _turn s)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; std_msgs/Header _header
     (send _header :deserialize buf ptr-) (incf ptr- (send _header :serialization-length))
   ;; uint8 _taskID
     (setq _taskID (sys::peek buf ptr- :char)) (incf ptr- 1)
   ;; string _taskName
     (let (n) (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4) (setq _taskName (subseq buf ptr- (+ ptr- n))) (incf ptr- n))
   ;; uint8 _taskPriority
     (setq _taskPriority (sys::peek buf ptr- :char)) (incf ptr- 1)
   ;; uint16 _forward
     (setq _forward (sys::peek buf ptr- :short)) (incf ptr- 2)
   ;; uint16 _turn
     (setq _turn (sys::peek buf ptr- :short)) (incf ptr- 2)
   ;;
   self)
  )

(setf (get kbot_bridge::MovementController :md5sum-) "5202306735651b0fbe3bb3c7dbbca1fd")
(setf (get kbot_bridge::MovementController :datatype-) "kbot_bridge/MovementController")
(setf (get kbot_bridge::MovementController :definition-)
      "#
Header header
uint8 taskID
string taskName
uint8 taskPriority
uint16 forward
uint16 turn

================================================================================
MSG: std_msgs/Header
# Standard metadata for higher-level stamped data types.
# This is generally used to communicate timestamped data 
# in a particular coordinate frame.
# 
# sequence ID: consecutively increasing ID 
uint32 seq
#Two-integer timestamp that is expressed as:
# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')
# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')
# time-handling sugar is provided by the client library
time stamp
#Frame this data is associated with
# 0: no frame
# 1: global frame
string frame_id

")



(provide :kbot_bridge/MovementController "5202306735651b0fbe3bb3c7dbbca1fd")


