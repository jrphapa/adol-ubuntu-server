(cl:in-package kbot_bridge-msg)
(cl:export '(HEADER-VAL
          HEADER
          TASKID-VAL
          TASKID
          TASKNAME-VAL
          TASKNAME
          TASKPRIORITY-VAL
          TASKPRIORITY
          FORWARD-VAL
          FORWARD
          TURN-VAL
          TURN
))