
(cl:in-package :asdf)

(defsystem "kbot_bridge-msg"
  :depends-on (:roslisp-msg-protocol :roslisp-utils :geometry_msgs-msg
               :sensor_msgs-msg
               :std_msgs-msg
)
  :components ((:file "_package")
    (:file "MovementController" :depends-on ("_package_MovementController"))
    (:file "_package_MovementController" :depends-on ("_package"))
    (:file "Power" :depends-on ("_package_Power"))
    (:file "_package_Power" :depends-on ("_package"))
    (:file "SonarPing" :depends-on ("_package_SonarPing"))
    (:file "_package_SonarPing" :depends-on ("_package"))
  ))