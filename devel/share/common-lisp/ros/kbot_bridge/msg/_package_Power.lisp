(cl:in-package kbot_bridge-msg)
(cl:export '(HEADER-VAL
          HEADER
          MAINVOLTAGE-VAL
          MAINVOLTAGE
          MAINVOLTAGEDIFF-VAL
          MAINVOLTAGEDIFF
          MAINCURRENT-VAL
          MAINCURRENT
          MAINCURRENTDIFF-VAL
          MAINCURRENTDIFF
          SECONDARYVOLTAGE-VAL
          SECONDARYVOLTAGE
          MAINSTATUS-VAL
          MAINSTATUS
          STATUS-VAL
          STATUS
          CHARGING-VAL
          CHARGING
))