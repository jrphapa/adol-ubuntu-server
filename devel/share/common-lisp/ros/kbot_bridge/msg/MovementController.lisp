; Auto-generated. Do not edit!


(cl:in-package kbot_bridge-msg)


;//! \htmlinclude MovementController.msg.html

(cl:defclass <MovementController> (roslisp-msg-protocol:ros-message)
  ((header
    :reader header
    :initarg :header
    :type std_msgs-msg:Header
    :initform (cl:make-instance 'std_msgs-msg:Header))
   (taskID
    :reader taskID
    :initarg :taskID
    :type cl:fixnum
    :initform 0)
   (taskName
    :reader taskName
    :initarg :taskName
    :type cl:string
    :initform "")
   (taskPriority
    :reader taskPriority
    :initarg :taskPriority
    :type cl:fixnum
    :initform 0)
   (forward
    :reader forward
    :initarg :forward
    :type cl:fixnum
    :initform 0)
   (turn
    :reader turn
    :initarg :turn
    :type cl:fixnum
    :initform 0))
)

(cl:defclass MovementController (<MovementController>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <MovementController>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'MovementController)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name kbot_bridge-msg:<MovementController> is deprecated: use kbot_bridge-msg:MovementController instead.")))

(cl:ensure-generic-function 'header-val :lambda-list '(m))
(cl:defmethod header-val ((m <MovementController>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader kbot_bridge-msg:header-val is deprecated.  Use kbot_bridge-msg:header instead.")
  (header m))

(cl:ensure-generic-function 'taskID-val :lambda-list '(m))
(cl:defmethod taskID-val ((m <MovementController>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader kbot_bridge-msg:taskID-val is deprecated.  Use kbot_bridge-msg:taskID instead.")
  (taskID m))

(cl:ensure-generic-function 'taskName-val :lambda-list '(m))
(cl:defmethod taskName-val ((m <MovementController>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader kbot_bridge-msg:taskName-val is deprecated.  Use kbot_bridge-msg:taskName instead.")
  (taskName m))

(cl:ensure-generic-function 'taskPriority-val :lambda-list '(m))
(cl:defmethod taskPriority-val ((m <MovementController>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader kbot_bridge-msg:taskPriority-val is deprecated.  Use kbot_bridge-msg:taskPriority instead.")
  (taskPriority m))

(cl:ensure-generic-function 'forward-val :lambda-list '(m))
(cl:defmethod forward-val ((m <MovementController>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader kbot_bridge-msg:forward-val is deprecated.  Use kbot_bridge-msg:forward instead.")
  (forward m))

(cl:ensure-generic-function 'turn-val :lambda-list '(m))
(cl:defmethod turn-val ((m <MovementController>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader kbot_bridge-msg:turn-val is deprecated.  Use kbot_bridge-msg:turn instead.")
  (turn m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <MovementController>) ostream)
  "Serializes a message object of type '<MovementController>"
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'header) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'taskID)) ostream)
  (cl:let ((__ros_str_len (cl:length (cl:slot-value msg 'taskName))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_str_len) ostream))
  (cl:map cl:nil #'(cl:lambda (c) (cl:write-byte (cl:char-code c) ostream)) (cl:slot-value msg 'taskName))
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'taskPriority)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'forward)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'forward)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'turn)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'turn)) ostream)
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <MovementController>) istream)
  "Deserializes a message object of type '<MovementController>"
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'header) istream)
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'taskID)) (cl:read-byte istream))
    (cl:let ((__ros_str_len 0))
      (cl:setf (cl:ldb (cl:byte 8 0) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'taskName) (cl:make-string __ros_str_len))
      (cl:dotimes (__ros_str_idx __ros_str_len msg)
        (cl:setf (cl:char (cl:slot-value msg 'taskName) __ros_str_idx) (cl:code-char (cl:read-byte istream)))))
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'taskPriority)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'forward)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'forward)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'turn)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'turn)) (cl:read-byte istream))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<MovementController>)))
  "Returns string type for a message object of type '<MovementController>"
  "kbot_bridge/MovementController")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'MovementController)))
  "Returns string type for a message object of type 'MovementController"
  "kbot_bridge/MovementController")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<MovementController>)))
  "Returns md5sum for a message object of type '<MovementController>"
  "5202306735651b0fbe3bb3c7dbbca1fd")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'MovementController)))
  "Returns md5sum for a message object of type 'MovementController"
  "5202306735651b0fbe3bb3c7dbbca1fd")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<MovementController>)))
  "Returns full string definition for message of type '<MovementController>"
  (cl:format cl:nil "#~%Header header~%uint8 taskID~%string taskName~%uint8 taskPriority~%uint16 forward~%uint16 turn~%~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')~%# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%# 0: no frame~%# 1: global frame~%string frame_id~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'MovementController)))
  "Returns full string definition for message of type 'MovementController"
  (cl:format cl:nil "#~%Header header~%uint8 taskID~%string taskName~%uint8 taskPriority~%uint16 forward~%uint16 turn~%~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')~%# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%# 0: no frame~%# 1: global frame~%string frame_id~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <MovementController>))
  (cl:+ 0
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'header))
     1
     4 (cl:length (cl:slot-value msg 'taskName))
     1
     2
     2
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <MovementController>))
  "Converts a ROS message object to a list"
  (cl:list 'MovementController
    (cl:cons ':header (header msg))
    (cl:cons ':taskID (taskID msg))
    (cl:cons ':taskName (taskName msg))
    (cl:cons ':taskPriority (taskPriority msg))
    (cl:cons ':forward (forward msg))
    (cl:cons ':turn (turn msg))
))
